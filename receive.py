import time
import zmq
import sys
import matplotlib.pyplot as plt 
import matplotlib.animation as animation
import numpy as np
import math 

def anim():
    global ax
    myfig,ax= plt.subplots()
    ani = animation.FuncAnimation(myfig,update, interval=10,blit=False)
    plt.show()
def update(_):
    global ax
    global socket1
    global socket2
    message1 = socket1.recv()
    new_message1 = message1.split()
    theta = float(new_message1[1])
    message2 = socket2.recv()
    new_message2 = message2.split()
    distance = float(new_message2[1])
    print 'polar coordinate', distance,theta
    x =  distance * math.sin(theta/180 * math.pi)
    y =  distance * math.cos(theta/180 * math.pi)
    print 'Cartesian coordinate', x,y
    point = ax.scatter(x,y,color='green')
    point.set_offsets(np.column_stack((x,y))) 
    return point,
# def receive_data(port=5556,topic=42):
#     context = zmq.Context()
#     socket = context.socket(zmq.SUB)
#     socket.setsockopt(zmq.SUBSCRIBE,"%d" % topic)
#     socket.connect("tcp://localhost:%s" % port)
    
#     while True:
#         print 'good'
#         socket.recv()
#         print 'good'

def image():
    i = 1
    while True:
        xcord = []
        ycord = []
        message1 = socket1.recv()
        new_message1 = message1.split()
        theta = float(new_message1[1])
        message2 = socket2.recv()
        new_message2 = message2.split()
        distance = float(new_message2[1])
        print 'polar coordinate', distance,theta
        x =  distance * math.sin(theta/180 * math.pi)
        y =  distance * math.cos(theta/180 * math.pi)
        print 'Cartesian coordinate', x,y
        xcord.append(x)
        ycord.append(y)
        while (i%20)==0:
            plt.plot(xcord,ycord)
            plt.xlabel('x axis')
            plt.ylabel('y axis')
            plt.title('Map')
            plt.savefig('Map %d'% (i/20))
        i += 1

def receive(port1 = 5556,topic1 = 42,port2 = 5557,topic2 = 42):
    global socket1
    global socket2
    context = zmq.Context()
    socket1 = context.socket(zmq.SUB)
    socket1.setsockopt(zmq.SUBSCRIBE,"%d" % topic1)
    socket1.connect("tcp://localhost:%s" % port1)
    socket2 = context.socket(zmq.SUB)
    socket2.setsockopt(zmq.SUBSCRIBE,"%d" % topic2)
    socket2.connect("tcp://localhost:%s" % port2)
    #anim()
    image()
    # while True:
    #     print 'good'
    #     socket.recv()

if __name__=="__main__":
    receive()

