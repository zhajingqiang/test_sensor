import smbus
import time
import zmq
import sys

def send_data(data,port=5556,topic=42):
        context = zmq.Context()
        print 'yes'
        socket = context.socket(zmq.PUB)
        socket.bind("tcp://*:%s"%port)
        socket.send("%d%d"%(topic,data))
        print 'send' 
        
        
if __name__=="__main__":
        channel = 0x01 # which i2c channel we use (other one is 0, 2 does not exist)
	address = 0x48 # i2c name of the sensor. Find it with i2cdetect -y 1
	bus = smbus.SMBus(channel)
	while True:
                y = bus.read_byte(address)
		x= 5.0/255 *y
		send_data(x,5556,42)
		print x
		time.sleep(1)


