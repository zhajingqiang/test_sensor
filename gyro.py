import smbus
import time
import zmq
import sys

def get_i2c_bus(channel = 1, address = 0x69):
        bus = smbus.SMBus(channel)
        REG1 = 0x20
        REG2 = 0x21
        REG3 = 0x22
        REG4 = 0x23
        REG5 = 0x24
        STAT = 0x27
	REG3_DEF = 0x08
        REG4_DEF = 0x08
        REG1_DEF = 0x1F
	# magic driver stuff
	bus.write_byte_data(address, REG3, REG3_DEF)
	bus.write_byte_data(address, REG4, REG4_DEF)
	bus.write_byte_data(address, REG1, REG1_DEF)
	return bus

def convert_raw(high,low):
	# deal with funky bit format for negatives #
	if high>127:
		return ((high-255)*256.0+(low-256.0)) * 8.75/ 1000 #/114.0
	else: 
		return ((high)*256.0+(low))* 8.75/ 1000 #/114.0
	
def gyro(port = 5556, topic = 42):
    WRITE_DATA = 0xD2
    READ_DATA = 0xD3
    
    OUT_X_INC = 0xA8

    
    channel = 0x01
    address = 0x69
    bus = get_i2c_bus(channel, address)
    context = zmq.Context()
    socket = context.socket(zmq.PUB)
    socket.bind("tcp://*:%s" % port)
    angle = 0
    dt = 0.01
    noise_z = 0
    i = 0
    while True:
            i +=1
            reg = bus.read_i2c_block_data(address, OUT_X_INC, 6)
            x=convert_raw(reg[1],reg[0])
            y=convert_raw(reg[3],reg[2])
            z=convert_raw(reg[5],reg[4])
            noise_z = (noise_z * (i-1)+ z)/ i
            angle += (z + 0.465) * dt
            #print x,y,z,reg
            #print noise_z
            print angle
            try:
                    socket.send("%d %d" % (topic,angle))
            except zmq.ZMQError:
                    print "..."
                    time.sleep(dt)

if __name__=="__main__":
	gyro()

