import RPi.GPIO as GPIO
import time
import zmq

def range_sensor(port = 5557,topic = 42):
        context = zmq.Context()
        socket = context.socket(zmq.PUB)
        socket.bind("tcp://*:%s" % port)

        GPIO.setmode(GPIO.BCM)
        TRIG = 23
        ECHO = 24
        print "Distance Measurement In Progress"

        GPIO.setup(TRIG,GPIO.OUT)
        GPIO.setup(ECHO,GPIO.IN)

        GPIO.output(TRIG, False)
        print "Waiting For Sensor To Settle"  
        time.sleep(2)
        dt = 0.05
        while True:
                GPIO.output(TRIG, True)

                time.sleep(0.00001)

                GPIO.output(TRIG, False)
                while GPIO.input(ECHO)==0:
                        pulse_start =time.time()
                        #print 'pulse_start',pulse_start
                while GPIO.input(ECHO)==1:
                        pulse_end = time.time()
                        #print 'pulse_end',pulse_end
                pulse_duration = pulse_end - pulse_start
                distance = pulse_duration *17150
                distance = round(distance, 2)
                socket.send("%d %d" % (topic,distance))  
                print "Distance:",distance,"cm"
                time.sleep(dt)
        GPIO.cleanup()

if __name__=="__main__":
        range_sensor()
